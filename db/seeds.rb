User.create!(name:  "Example User",
             email: "example@7music.io",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@7music.io"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end


users = User.order(:created_at).take(5)
50.times do |n|
  title = Faker::Name.name
  description = Faker::Lorem.sentence(5)
  genre_tag = "Techno"
  genre_tag2 = "House"
  users.each { |user| user.projects.create!(title: title, description: description, 
                                            genre_tag: genre_tag, genre_tag2: genre_tag2) }
end

# Following relationships
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }