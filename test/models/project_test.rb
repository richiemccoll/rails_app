require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    # This code is not idiomatically correct.
    @project = @user.projects.build(title: "Lorem ipsum", genre_tag: "house", genre_tag2: "techno", 
    					   description: "this is a test project", picture: "picture.png")
  end

  test "should be valid" do
    assert @project.valid?
  end

  test "user id should be present" do
    @project.user_id = nil
    assert_not @project.valid?
  end

  test "title should be present" do
    @project.title = "     "
    assert_not @project.valid?
  end
  
    test "all content should be present" do
    @project.title = "   "
    @project.genre_tag = "test"
    @project.description = "   "
    @project.picture = "  "
    assert_not @project.valid?
  end

  test "description should be at most 140 characters" do
    @project.description = "a" * 141
    assert_not @project.valid?
  end

  test "title should be at most 25 characters" do
    @project.title = "a" * 26
    assert_not @project.valid?
  end

  test "genre_tag should be at most 25 characters" do
    @project.genre_tag = "a" * 26
    assert_not @project.valid?
  end

  test "order should be most recent first" do
    assert_equal projects(:most_recent), Project.first
  end

end
