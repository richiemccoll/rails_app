class ProjectsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]


  def new
      @project = current_user.projects.build if logged_in?
  end
  
  def create
  	@project = current_user.projects.build(project_params)
    if @project.save
      flash[:success] = "Project created successfully!"
      redirect_to root_url
    else
    @feed_items = []
      render 'users/show'
    end
  end

   def destroy
    @project.destroy
    flash[:success] = "Your Project Has Been Deleted"
    redirect_to request.referrer || root_url
  end

  private

    def project_params
      params.require(:project).permit(:title, :description, :genre_tag, :genre_tag2, :picture)
    end

    def correct_project
      @project = current_user.projects.find_by(id: params[:id])
      redirect_to root_url if @project.nil?
    end
end
