require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @user2 = users(:test2)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { name:  "",
                                    email: "foo@invalid",
                                    password:              "foo",
                                    password_confirmation: "bar" }
    assert_template 'users/edit'
  end

  test "successful edit" do
    log_in_as(@user2)
    get edit_user_path(@user2)
    assert_template 'users/edit'
    name  = "Test Me"
    email = "test1@test.com"
    full_name = "Test me you cant"
    country = "uk"
    city = "glasgow"
    website_link = "www.mixcloud.com"
    website_link2 = "www.yupty.com"
    genre_tag = "house"
    daw = "ableton"
    patch user_path(@user2), user: { name:  name,
                                    email: email,
                                	full_name: full_name,
                                	country: country,
                                	city: city,
                                	website_link: website_link,
                                	website_link2: website_link2,
                                	genre_tag: genre_tag,
                                	daw: daw,
                                	password:              "",
                                    password_confirmation: "" 
                                	}
    assert_not flash.empty?
    assert_redirected_to @user2
    @user2.reload
    assert_equal name,  @user2.name
    assert_equal email, @user2.email
    assert_equal full_name, @user2.full_name
    assert_equal country, @user2.country
    assert_equal city, @user2.city
    assert_equal website_link, @user2.website_link
    assert_equal website_link2, @user2.website_link2
    assert_equal genre_tag, @user2.genre_tag
    assert_equal daw, @user2.daw
  end

  test "successful full profile edit with friendly forwarding" do
    get edit_user_path(@user2)
    log_in_as(@user2)
    assert_redirected_to edit_user_path(@user2)
    name  = "Test Me"
    email = "test1@test.com"
    full_name = "Test me you cant"
    country = "uk"
    city = "glasgow"
    website_link = "www.mixcloud.com"
    website_link2 = "www.yupty.com"
    genre_tag = "house"
    daw = "ableton"
    patch user_path(@user2), user: { name:  name,
                                    email: email,
                                    full_name: full_name,
                                    country: country,
                                    city: city,
                                    website_link: website_link,
                                    website_link2: website_link2,
                                    genre_tag: genre_tag,
                                    daw: daw,
                                    password:              "",
                                    password_confirmation: "" 
                                    }
    assert_not flash.empty?
    assert_redirected_to @user2
    @user2.reload
    assert_equal name,  @user2.name
    assert_equal email, @user2.email
    assert_equal full_name, @user2.full_name
    assert_equal country, @user2.country
    assert_equal city, @user2.city
    assert_equal website_link, @user2.website_link
    assert_equal website_link2, @user2.website_link2
    assert_equal genre_tag, @user2.genre_tag
    assert_equal daw, @user2.daw
  end

  test "successful partial edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_path(@user)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @user
    @user.reload
    assert_equal name,  @user.name
    assert_equal email, @user.email
  end
end
