class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :full_name
      t.string :profile_url
      t.string :avatar_url
      t.string :country
      t.string :city
      t.integer :project_count
      t.integer :contributions_count
      t.integer :followers_count
      t.integer :followings_count
      t.string :website_link
      t.string :website_link2
      t.string :genre_tag
      t.string :daw

      t.timestamps null: false
    end
  end
end
